/**
 * @file   can_driver.h
 * @brief  CAN驱动程序，主要包含CAN数据接收发送函数
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-10-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#ifndef __CAN_DRIVER_H
#define __CAN_DRIVER_H
#include "stm32f4xx.h"
/**
 * @brief  CAN波特率参数定义
 */
typedef  struct {
  unsigned int    BaudRate;//CAN波特率值，单位为bps
  unsigned char   SJW;
  unsigned char   BS1;
  unsigned char   BS2;
  unsigned short  PreScale;
}CAN_BAUDRATE;
/**
 * @brief  CAN消息结构体定义
 */
typedef  struct {
  unsigned int ID;
  struct {
    unsigned char IDE:1;///<0-标准帧，1-扩展帧 
    unsigned char RTR:1;///<0-数据帧，1-远程帧
    unsigned char DIR:1;///<0-接收数据帧，1-发送数据帧
    unsigned char DLC:4;
  }Flags;
  unsigned char  Data[8];
}CAN_MSG;
/**
 * @brief  CAN初始化配置
 * @param  Channel CAN通道号
 * @param  BaudRate CAN波特率，单位为bps
 */
void CAN_Configuration(uint8_t Channel,uint32_t BaudRate);

/**
 * @brief  发送CAN数据到总线
 * @param  Channel CAN通道号
 * @param  TxMessage CAN消息指针
 * @return 
 */
uint8_t CAN_WriteData(uint8_t Channel,CAN_MSG *pMsg);

#endif

