/**
 * @file   can_boot.h
 * @brief  CAN Boot头文件
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-10-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#ifndef __CAN_BOOT_H
#define __CAN_BOOT_H
#include "main.h"
#include "can_driver.h"

#define BOOT_START_ADDR         0x08000000  //BOOT程序起始地址
#define APP_START_ADDR          0x08008000  //APP程序起始地址
#define APP_VALID_FLAG_ADDR     0x08004000  //APP程序有效标志存储地址
#define BOOT_REQ_FLAG_ADDR      0x08004010  //刷新请求标志存储地址
//标志定义
#define APP_VALID_FLAG  0x55AA55AA
#define BOOT_REQ_FLAG   0xAA55AA55
//固件类型值定义
#define FW_TYPE_BOOT     0x55
#define FW_TYPE_APP      0xAA
//定义当前固件类型为BOOT
#define FW_TYPE         FW_TYPE_BOOT

uint8_t CAN_BOOT_GetProgRequest(void);
void CAN_BOOT_SetProgRequest(void);
void CAN_BOOT_ResetProgRequest(void);
void CAN_BOOT_Reset(void);
void CAN_BOOT_ExeApp(void);
uint8_t CAN_BOOT_EraseApp(void);
uint8_t CAN_BOOT_CheckApp(void);
void CAN_BOOT_SetAppValid(void);
uint8_t CAN_BOOT_WriteDataToFlash(uint32_t Addr,uint8_t *pData,uint32_t DataLen);

uint32_t GetSector(uint32_t Address);
uint8_t CAN_BOOT_ErasePage(void);
uint8_t CAN_BOOT_ProgramDatatoFlash(uint32_t StartAddr,uint8_t *pData,uint32_t DataNum);
void CAN_BOOT_JumpToApplication(__IO uint32_t Addr);

#endif


