/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  *
 ******************************************************************************
 *  $File Name$       : main.c                                         	      *
 *  $Author$          : ChipON AE/FAE Group                                   *
 *  $Data$            : 2021-07-8                                             *
 *  $Hard Version     : KF32A156-MINI-EVB_V1.2                                *
 *  $Description$     : Provide the operation interface of data area flash,   *
 * 						including reading, erasing, page writing, byte 		  *
 * 						writing, comparison and other functions		  		  *	
 ******************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 *
 *  All rights reserved.                                                      *
 *                                                                            *
 *  This software is copyright protected and proprietary to                    *
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 *
 ******************************************************************************
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *                     		REVISON HISTORY                               	  *
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  Data       Version  Author        Description                             *
 *  ~~~~~~~~~~ ~~~~~~~~ ~~~~~~~~~~~~  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  2021-07-08 00.02.00 FAE Group     Version 2.0 update                      *
 *                                                                            *
 *                                                                            *
 *****************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "Usart.h"
#include "stdio.h"
#include "Flash.h"
#include "ChipMessageApi.h"
#include "can_driver.h"
#include "can_uds.h"
#include "can_buffer.h"
#include "can_boot.h"
/*******************************************************************************
**                      	Private Macro Definitions                         **
*******************************************************************************/
/******************************************************************************
**                   	Private variables  Definitions                       **
******************************************************************************/
//CAN 消息FIFO变量
extern CAN_BUFFER  CANRxBuffer[2];
/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
	/**
 *  @brief :Initializes the GPIO of the Usart
 *  @param in :None
 *  @param out :None
 *  @retval :PA3 -- USART0_TX0
 *           PE7 -- USART0_RX
 */
	void
	Usart_Gpio_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	/*
   *  Configure PA3 remap mode
   *  as TX pin
   */
	GPIO_InitStructure.m_Mode = GPIO_MODE_RMP;
	GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;
	GPIO_InitStructure.m_Pin = GPIO_PIN_MASK_3;
	GPIO_Configuration(GPIOA_SFR, &GPIO_InitStructure);
	/*
   * Configure PE7 remap mode
   * as RX pin
   * Configured in pull-up mode
   */
	GPIO_InitStructure.m_PullUp = GPIO_PULLUP;
	GPIO_InitStructure.m_Pin = GPIO_PIN_MASK_7;
	GPIO_Configuration(GPIOE_SFR, &GPIO_InitStructure);
	/* Configure PA3 remap function to AF3 */
	GPIO_Pin_RMP_Config(GPIOA_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF3);
	/* Configure PE7 remap function to AF3 */
	GPIO_Pin_RMP_Config(GPIOE_SFR, GPIO_Pin_Num_7, GPIO_RMP_AF3);
}

/*******************************************************************************
**                     			main Functions 		             	     	  **
*******************************************************************************/
int main()
{
	uint32_t i, FlashRetVal;
	/* Initialize the system clock is 120M */
	SystemInit(120);
	/* Setup SysTick Timer as delay function, and input frequency is 120M */
	systick_delay_init(120);
	/* Initialize the USART IOs */
	Usart_Gpio_Init();
	/* USARTx configured as follow:
		- BaudRate = 115200 baud
		- Word Length = 8 Bits
		- One Stop Bit
		- No parity
		- Hardware flow control disabled (RTS and CTS signals)
		- Receive and transmit enabled
	 */
	USART_Async_config(USART0_SFR);
	printf("---------------------------------------------------------------------\r\n");
	printf("Start CAN UDS Bootloader\r\n");
	if(!CAN_BOOT_GetProgRequest()){
		if(*((uint32_t *)APP_VALID_FLAG_ADDR)==APP_VALID_FLAG){
		  printf("Exe App...\r\n");
		  systick_delay_ms(10);
		  CAN_BOOT_ExeApp();
		}
	}
	printf("Exe Boot...\r\n");
	CAN_BUF_Init(0);
	CAN_Configuration();
	printf("CAN1 Config End\r\n");
	printf("APP_VALID_FLAG = 0x%08X\r\n",*((uint32_t *)APP_VALID_FLAG_ADDR));
	printf("BOOT_REQ_FLAG = 0x%08X\r\n",*((uint32_t *)BOOT_REQ_FLAG_ADDR));
	while (1)
	{
	    CAN_MSG *pMsg;
	    if(CAN_BUF_PopMsg(0,&pMsg,1)){
#if DEBUG_SHOW_CAN_MSG
	      printf("%s CAN(0x%08X):",((pMsg->Flags.DIR)?"TX":"RX"),pMsg->ID);
	      for(int i=0;i<pMsg->Flags.DLC;i++){
	        printf("%02X ",pMsg->Data[i]);
	      }
	      printf("\r\n");
#endif
	      if(pMsg->Flags.DIR){
	        CAN_WriteData(0,pMsg);
	      }else{
	        CAN_UDS_Process(pMsg->Data,pMsg->Flags.DLC);
	      }
	    }
	}
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
	/* User can add his own implementation to report the file name and line number,
		ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
		;
	}
};
